import click
from pathlib import Path
import os
import sys
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
import pickle
from tensorflow import keras

src_dir = Path(__file__).resolve().parent
project_dir = src_dir.parent

sys.path.append(src_dir)

from train.regression.modelling import predict_model, get_anomalies
from train.rules.rule_based_filtering import find_duplicates, get_anomalies_by_rules

#Attributs
# dataset_file='../data/cas1/beauharnois_aval_2015_2022_brutes.xlsx'
# model_file='../models/gan_disc'

@click.command()
@click.argument('dataset_file', type=click.Path(exists=True), required=False, default=os.path.join(project_dir, 'data/cas1/parsed.csv'))
@click.argument('gan_model_file', type=click.Path(exists=True), required=False, default=os.path.join(project_dir, 'models/gan_disc'))
@click.argument('regression_model_file', type=click.Path(exists=True), required=False, default=os.path.join(project_dir, 'models/cas1/regression_model_aval.pkl'))
@click.argument('year_start', type=int, required=False, default=2015)
def main(dataset_file, gan_model_file, regression_model_file, year_start):
    # Read dataset and keep only dates of intereset
    print('Loading dataset')
    df = pd.read_csv(dataset_file, parse_dates=['Date'])
    date_start = datetime(year_start, 1, 1)
    df = df[df.Date >= date_start]

    results = {}

    # Rules
    print('Getting anomalies by rules')
    df = get_anomalies_by_rules(df, 'aval_brute')

    size=288
    overlap=0
    date_data=df.index.values
    date_split = [date_data[i:i+size] for i in range(0, len(date_data),size-overlap) if i <len(date_data)-overlap]
    results['Date'] = [split[0] for split in date_split]

    for anomaly in ['is_negative', 'is_missing', 'is_frozen', 'is_peak', 'is_duplicate']:
        results[anomaly] = [df.loc[split][anomaly].max() for split in date_split]


    # Regression
    print('Getting anomalies by regression')
    with open(regression_model_file, 'rb') as f:
        model_reg = pickle.load(f)
    df['month'] = df.index.month
    df['regression_predictions'] = predict_model(model_reg, df, ['month', 'quai_brute', 'rain1', 'rain2'])
    results['regression_anomalies'] = [
        get_anomalies(df.loc[split].reset_index(), 'regression_predictions', 'aval_brute').max() 
        for split in date_split
    ]

    ############   GAN   #################
    print('Getting anomalies by GAN')
    data=df[f'aval_brute'].fillna(method='ffill').values
    
    #normalization
    data=data-np.mean(data)
    #On complète le dernier élément incomplet
    # data=np.pad(data, (0, size-(len(data) % size)))
    #transform timeserires into daily numpy tab
    data_split = [data[i:i+size] for i in range(0, len(data),size-overlap) if i <len(data)-overlap]

    
    data_split[-1]=data_split[-2]
    
    data_split=np.array(data_split)
    date_split=np.array(date_split)
    
    #Load Keras model
    model = keras.models.load_model(gan_model_file)

    #prediction
    res_GAN=model.predict(data_split.reshape(len(data_split),1,288))
    
    ############   Anomalies réelles   #################
    data=df[f'aval_brute'].values
    data_valid=df[f'aval_validee'].values
     
    #transform timeserires into daily numpy tab
    size=288
    overlap=0
    data_split = [data[i:i+size] for i in range(0, len(data),size-overlap) if i <len(data)-overlap]
    data_valid_split = [data_valid[i:i+size] for i in range(0, len(data_valid),size-overlap) if i <len(data_valid)-overlap]

    #On enlève le dernier élément incomplet
    # data_split=np.array(data_split[:-1])
    # data_valid_split=np.array(data_valid_split[:-1])
    
    results['Real_detection']=[1*(sum(abs(data_split[i]-data_valid_split[i])>0.1)>0) for i in range(len(data_split))]
                              

    results['GAN_detection'] = np.where(res_GAN.reshape(-1) < 0.4, 1, 0)
    
    gan_confidence = -np.ones(len(res_GAN))
    for confidence, threshold in zip([0, 1, 2], [0.4, 0.3, 0.2]):
        gan_confidence[res_GAN.reshape(-1) < threshold] = confidence
    results['GAN_confidence'] = gan_confidence

    # results['']
    # #Création du tableau
    # tab_res=[]
    # for i in range(len(res_GAN)):
         
    #     if res_GAN[i]<0.2:
    #         gan_detection=1
    #         gan_confidence=2
    #     elif res_GAN[i]<0.3:
    #         gan_detection=1
    #         gan_confidence=1
    #     elif res_GAN[i]<0.4:
    #         gan_detection=1
    #         gan_confidence=0
    #     else:
    #         gan_detection=0
    #         gan_confidence=-1

            
    #     tab_res.append([str(date_split[i][0]),gan_detection,gan_confidence,res_real[i]])
            
    # df_res = pd.DataFrame(tab_res, columns=['Date', 'GAN_detection','GAN_confidence','Real_detection'])

    df_res = pd.DataFrame(results)

    df_res.to_csv('../results/Result.csv')
    
if __name__ == '__main__':
    
    main()


