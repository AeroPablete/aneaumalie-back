import lightgbm as lgb
import numpy as np
from tqdm import tqdm
from sklearn.metrics import mean_squared_error



def train_model(train_df, feature_cols, target_col, params={}):
    clf = lgb.LGBMRegressor(**params)
    clf.fit(train_df[feature_cols], train_df[target_col])
    return clf


def predict_model(model, df, feature_cols):
    return model.predict(df[feature_cols])


def tune_hyperparams(param_grid, df_train, df_val, feature_cols, y_col):
    best_model = None
    best_metric = np.inf
    best_params = None
    for params in tqdm(list(param_grid)):
        model = train_model(df_train, feature_cols, y_col, params)
        pred = predict_model(model, df_val, feature_cols)
        mse = mean_squared_error(df_val[y_col], pred)
        if mse < best_metric:
                best_metric = mse
                best_model = model
                best_params = params
    return best_model, best_metric, best_params


def get_anomalies(df, col_pred, col_real, date_col='Date', window_size=3, threshold=0.375):
    df[f'diff_{col_pred}_{col_real}'] = np.abs(df[col_pred] - df[col_real])
    rolling_diff = df.set_index(date_col)[f'diff_{col_pred}_{col_real}'].rolling(window_size).mean()
    alert = rolling_diff > threshold
    return alert