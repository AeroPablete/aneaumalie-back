import numpy as np


def find_duplicates(df, column, date_column='Date'):
    return (df.groupby(date_column)[column].nunique() > 1).rename('is_duplicate')


def find_negative_values(df, column):
    return df[column] < 0


def find_missing_values(df, column):
    return df[column].isna()


def find_frozen_values(df, column, window_size='2H'):
    return df.rolling(window_size)[column].std() == 0


def find_peaks(df, column, threshold=6, window_size='1H'):
    with_noise = df[column] + np.random.normal(0,0.1,size=len(df))
    rolling_mean = with_noise.shift(1).rolling(window_size).mean()
    rolling_std = with_noise.shift(1).rolling(window_size).std()
    is_peak = (np.abs(df[column] - rolling_mean)/rolling_std) > threshold
    
    return is_peak


def get_anomalies_by_rules(df, column, date_column='Date', expected_freq = '5min'):
    is_duplicate = find_duplicates(df, column, date_column)
    df_resampled = df.set_index(date_column).resample(expected_freq).min()
    df_resampled['is_negative'] = find_negative_values(df_resampled, column)
    df_resampled['is_missing'] = find_missing_values(df_resampled, column)
    df_resampled['is_frozen'] = find_frozen_values(df_resampled, column)
    df_resampled['is_peak'] = find_peaks(df_resampled, column)

    df_resampled = df_resampled.merge(is_duplicate, left_index=True, right_index=True, how='left')
    df_resampled.is_duplicate = df_resampled.is_duplicate.fillna(False)
    return df_resampled