## Project Organization
    
    ├── requirements.txt   <- File to install dependencies.
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── cas1           <- Data for the first use case
    │   │   ├── beauharnois_aval_2007_2015_brutes.xlsx
    │   │   └── ...
    │   ├── cas2           <- Data for the second use case
    │   └── cas3           <- Data for the last use case
    │
    ├── models             <- Trained and serialized models & model predictions.
    │
    └── src                <- Source code for use in this project.
        │
        ├── data           <- Code to parse data
        │   ├── make_dataset.py
        │   └── ...
        │
        ├── regression      <- Code for the regression model
        │   ├── train_model.py
        │   └── ...
        │
        └── gan            <- Code for the GAN model
            ├── train_model.py
            └── ...
  

